<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePalimpsestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('palimpsests', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->unsignedBigInteger('user_id');
            $table->string('name')->default('');
            $table->string('driver')->default('');
            $table->string('config')->default('');

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });

        Schema::create('palimpsest_record', function (Blueprint $table) {
            $table->unsignedBigInteger('palimpsest_id');
            $table->unsignedBigInteger('record_id');
            $table->unsignedInteger('sorting')->default(0);
            $table->string('notes')->default('');

            $table->foreign('record_id')->references('id')->on('records')->onDelete('cascade');
            $table->foreign('palimpsest_id')->references('id')->on('palimpsests')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('palimpsests');
    }
}
