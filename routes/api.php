<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('api')->group(function() {
    Route::get('record/init', 'RecordController@init')->name('record.init');
    Route::get('record/start', 'RecordController@start')->name('record.start');
    Route::get('record/act/{id}', 'RecordController@act')->name('record.act');
    Route::get('record/stop/{id}', 'RecordController@stop')->name('record.stop');
    Route::delete('record/delete/{id}', 'RecordController@delete')->name('record.delete');
});
