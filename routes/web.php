<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['register' => false]);

Route::get('css/{filename}', 'AssetsController@css')->where('filename', '(.*)')->name('assets.css');
Route::get('js/{filename}', 'AssetsController@js')->where('filename', '(.*)')->name('assets.js');
Route::get('fonts/{filename}', 'AssetsController@fonts')->where('filename', '(.*)')->name('assets.fonts');

Route::middleware('auth')->group(function() {
    Route::get('home', 'HomeController@index')->name('home');

    Route::get('palimpsest/record/{palimpsest}/{record}', 'PalimpsestController@record')->name('palimpsest.record');
    Route::post('palimpsest/records/{palimpsest}', 'PalimpsestController@handleRecords')->name('palimpsest.records');
    Route::post('palimpsest/loopback/{palimpsest}/{record_identifier}/{status}', 'PalimpsestController@loopback')->name('palimpsest.loopback');
    Route::post('palimpsest/broadcast/{palimpsest}/{record_identifier}/{status}', 'PalimpsestController@broadcast')->name('palimpsest.broadcast');
    Route::get('palimpsest/askremove/{palimpsest}', 'PalimpsestController@askRemove')->name('palimpsest.ask_remove');

    Route::get('record/askremove/{record:identifier}', 'RecordController@askRemove')->name('record.ask_remove');

    Route::resource('record', 'RecordController');
    Route::resource('palimpsest', 'PalimpsestController');
});
