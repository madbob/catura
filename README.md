## Catura

Catura is a web service to asyncronously video capture a given URL. It wraps Chromium browser and GStreamer, and provides both an API and a web UI to start and stop the recording.

## Requirements

 * php >= 7.2 and related Laravel dependencies
 * mysql-server
 * gstreamer1.0-tools
 * gstreamer1.0-pulseaudio
 * gstreamer1.0-plugins-bad
 * gstreamer1.0-plugins-ugly
 * xdotool
 * xvfb
 * xinit
 * dbus-x11
 * pulseaudio
 * pulseaudio-utils
 * chromium-browser
 * xserver-xorg-video-dummy
 * nginx
 * libnginx-mod-rtmp

## Installation

```
git clone https://gitlab.com/madbob/catura.git
cd catura
composer install
npm install
cp .env.example .env
[edit the contents of .env to configure your database connection]
php artisan migrate
```

To handle different video and audio sessions, it is required to have an X session and PulseAudio running on the server.

Choose or create a non-root user, from here we call it "foobar"

Create a file `/home/foobar/xinit.sh` containing

```
#!/bin/sh

dbus-launch
sleep 1
pulseaudio --start

sleep 365d
```

Create the file `/etc/systemd/system/xsession.service` containing

```
[Unit]
Description=xinit - user "foobar"
[Service]
User=foobar
Group=foobar
ExecStart=/usr/bin/xinit /home/foobar/xinit.sh -- /usr/bin/Xvfb :1 -nolisten tcp
ExecStopPost=/usr/bin/killall -u foobar
[Install]
WantedBy=multi-user.target
```

Execute

```
systemctl daemon-reload
systemctl enable xsession
systemctl start xsession
```

From here, different Chromium sessions will be opened on different DISPLAYs of the X session and will be attached to unique PulseAudio virtual devices, so to avoid collisions among different audio and video flows.

Once the system is installed, execute

 * `php artisan user:create foobar foo@bar.baz` to create a new user and generate a new password and API key
 * `php artisan server:run` to execute the server (by default it listen on the port 80). This has to run with the same user of the X session created above ("foobar", in the example)

## Loopback

The UI permits to display a preview of the captured contents, but expects a local RTMP server to be used as an endpoint.

Configure a local nginx instance as following (in `/etc/nginx/nginx.conf`);

```
http {
    sendfile off;
    tcp_nopush on;
    directio 512;
    default_type application/octet-stream;

    server {
        listen 2222;

        location /hls {
            add_header Cache-Control no-cache;
            add_header 'Access-Control-Allow-Origin' '*' always;
            add_header 'Access-Control-Expose-Headers' 'Content-Length';

            if ($request_method = 'OPTIONS') {
                add_header 'Access-Control-Allow-Origin' '*';
                add_header 'Access-Control-Max-Age' 1728000;
                add_header 'Content-Type' 'text/plain charset=UTF-8';
                add_header 'Content-Length' 0;
                return 204;
            }

            types {
                application/vnd.apple.mpegurl m3u8;
                video/mp2t ts;
            }

            root /tmp/hls;
        }
    }
}

rtmp {
    server {
        listen 1935;
        ping 30s;
        notify_method get;

        application live {
            live on;
            hls on;
            hls_path /tmp/hls/hls;
            hls_sync 100ms;
            hls_fragment 3;
            hls_playlist_length 60;
            allow play all;
        }
    }
}
```

## Troubleshooting

Run `xdg-settings set default-web-browser chromium-browser.desktop` to avoid the "Chromium is not the default browser" bar fixed on the top of the browser's window.

### License

Catura is distributed under the AGPLv3 License.

Copyright (C) 2020 Roberto Guido <bob@linux.it>
