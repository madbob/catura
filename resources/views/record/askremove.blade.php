<div class="modal fade" id="remove-record-{{ $record->id }}" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Remove Record</h5>
                <button type="button" class="close" data-dismiss="modal">
                    <span>&times;</span>
                </button>
            </div>
            <form method="DELETE" action="{{ route('record.delete', $record->identifier) }}" class="reload_on_submit">
                <input type="hidden" name="api_key" value="{{ $user->api_key }}">

                <div class="modal-body">
                    <p>
                        Are you sure to remove this record?
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger">Delete</button>
                </div>
            </form>
        </div>
    </div>
</div>
