<div class="card mb-2" data-record-id="{{ $record->identifier }}">
    <div class="card-header">
        <ul class="nav nav-pills card-header-pills">
            <li class="nav-item">
                <a class="nav-link remote-modal" href="#" data-modal-url="{{ route('palimpsest.record', ['palimpsest' => $record->palimpsest->first()->id, 'record' => $record->identifier]) }}">
                    <i class="icofont icofont-gear"></i>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link remote-modal" href="#" data-modal-url="{{ route('record.ask_remove', $record->identifier) }}">
                    <i class="icofont icofont-minus-square"></i>
                </a>
            </li>
        </ul>
    </div>

    @if($record->loopback_url)
        <video autoplay controls id="loopback-record-{{ $record->id }}" class="video-js">
            <source src="{{ $record->loopback_url }}" type="application/x-mpegURL">
        </video>
    @else
        <div class="record-card-status">
            <i class="icofont {{ $record->status_icon }}"></i>
        </div>
    @endif

    <div class="card-body">
        <p class="card-text">
            {{ $record->url }}<br>
            {{ $record->pivot->notes ?: 'No notes' }}
        </p>

        <button type="button" class="btn btn-warning {{ $record->loopback_url ? 'active' : '' }} do_loopback" data-toggle="button" aria-pressed="{{ $record->loopback_url ? 'true' : 'false' }}">
            Loopback
        </button>

        <button type="button" class="btn btn-primary {{ $record->is_broadcasting ? 'active' : '' }} do_broadcast" data-toggle="button" aria-pressed="{{ $record->loopback_url ? 'true' : 'false' }}">
            Broadcast
        </button>
    </div>
</div>
