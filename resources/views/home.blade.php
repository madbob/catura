@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row mb-5">
        <div class="col-md-12">
            <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#edit-record-new">Create New Record</a>
            @include('record.edit', ['record' => null])

            <a href="{{ route('palimpsest.index') }}" class="btn btn-secondary">Go to Palimpsest View</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Latest Records</div>

                <div class="card-body">
                    @if($user->records()->count() == 0)
                        <div class="alert alert-info">
                            There are no records.
                        </div>
                    @else
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Identifier</th>
                                    <th>URL</th>
                                    <th>Created At</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($user->records()->take(10)->get() as $record)
                                    <tr>
                                        <td>{{ $record->identifier }}</td>
                                        <td>{{ $record->url }}</td>
                                        <td>{{ $record->created_at }}</td>
                                        <td>
                                            @if($record->status == 'running')
                                                <a href="{{ route('record.stop', ['id' => $record->id, 'api_key' => $user->api_key]) }}" class="btn btn-danger reload_on_click">Stop Record</a>
                                            @endif

                                            <a href="#" class="btn btn-danger remote-modal" data-modal-url="{{ route('record.ask_remove', $record->identifier) }}">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
