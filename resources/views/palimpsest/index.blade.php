@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row mb-5">
        <div class="col-md-12">
            <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#edit-palimpsest-new">Create New Palimpsest</a>
            @include('palimpsest.edit', ['palimpsest' => null])

            <a href="{{ route('home') }}" class="btn btn-secondary">Go to Records List</a>
        </div>
    </div>
    <div class="row">
        @foreach($user->palimpsests as $palimpsest)
            <div class="col-md-3">
                <div class="card" data-palimpsest-id="{{ $palimpsest->id }}">
                    <div class="card-header">
                        <span class="float-right">
                            <a href="#" class="btn btn-primary add-to-palimpsest" data-toggle="modal" data-target="#edit-palimpsest-{{ $palimpsest->id }}-record-new">
                                <i class="icofont icofont-plus-square"></i>
                            </a>
                            @include('palimpsest.record', ['palimpsest' => $palimpsest, 'record' => null])

                            <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#edit-palimpsest-{{ $palimpsest->id }}">
                                <i class="icofont icofont-gear"></i>
                            </a>
                            @include('palimpsest.edit', ['palimpsest' => $palimpsest])
                        </span>
                        {{ $palimpsest->name }}

                        <br>
                        <br>

                        <video autoplay controls id="loopback-palimpsest-{{ $palimpsest->id }}" class="video-js">
                            <source src="{{ $palimpsest->subConfig('readfrom') }}" type="application/x-mpegURL">
                        </video>
                    </div>

                    <div class="card-body">
                        @if($palimpsest->records()->count() == 0)
                            <div class="alert alert-info">
                                There are no records.
                            </div>
                        @else
                            @foreach($palimpsest->records as $record)
                                @include('record.cell', ['record' => $record])
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>

@endsection
