<div class="modal fade" id="remove-palimpsest-{{ $palimpsest->id }}" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Remove Palimpsest</h5>
                <button type="button" class="close" data-dismiss="modal">
                    <span>&times;</span>
                </button>
            </div>
            <form method="POST" action="{{ route('palimpsest.destroy', $palimpsest->id) }}">
                @csrf
                @method('DELETE')

                <div class="modal-body">
                    <p>
                        Are you sure to remove this palimpsest and all included records?
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger">Delete</button>
                </div>
            </form>
        </div>
    </div>
</div>
