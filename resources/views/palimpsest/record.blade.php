<div class="modal fade" id="edit-palimpsest-{{ $palimpsest->id }}-record-{{ $record ? $record->id : 'new' }}" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Record</h5>
                <button type="button" class="close" data-dismiss="modal">
                    <span>&times;</span>
                </button>
            </div>
            <form method="POST" action="{{ route('palimpsest.records', [$palimpsest->id]) }}" class="reload_on_submit">
                @csrf

                @if($record)
                    <input type="hidden" name="identifier" value="{{ $record->identifier }}">
                @endif

                <div class="modal-body">
                    <div class="form-group">
                        <label for="url">URL</label>
                        <input type="text" class="form-control" id="url" name="url" value="{{ $record ? $record->url : '' }}">
                    </div>
                    <div class="form-group">
                        <label for="setup">Initial Setup</label>
                        <textarea class="form-control" id="setup" name="setup">{{ $record ? $record->setup : '' }}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="notes">Notes</label>
                        <textarea class="form-control" id="notes" name="notes">{{ $record ? $record->setup : '' }}</textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
