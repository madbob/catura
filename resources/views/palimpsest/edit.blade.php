<div class="modal fade" id="edit-palimpsest-{{ $palimpsest ? $palimpsest->id : 'new' }}" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Palimpsest</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="{{ $palimpsest ? route('palimpsest.update', $palimpsest->id) : route('palimpsest.store') }}">
                @csrf

                @if($palimpsest)
                    @method('PUT')
                @endif

                <div class="modal-body">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" id="name" name="name" value="{{ $palimpsest ? $palimpsest->name : '' }}">
                    </div>
                    <div class="form-group">
                        <label for="setup">Driver</label>
                        <select class="form-control" name="driver" required>
                            <option value="">Choose one</option>
                            @foreach(App\Destination::drivers() as $driver)
                                <option value="{{ $driver->identifier }}" {{ $palimpsest && $palimpsest->driver == $driver->identifier ? 'selected' : '' }}>{{ $driver->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    @foreach(App\Destination::drivers() as $driver)
                        <div class="params_block params_block_{{ $driver->identifier }} {{ $palimpsest && $palimpsest->driver == $driver->identifier ? '' : 'hidden' }}">
                            @foreach($driver->params as $identifier => $param)
                                <div class="form-group">
                                    <label for="config">{{ $param->label }}</label>
                                    <input type="text" class="form-control" id="{{ $driver->identifier }}_{{ $identifier }}" name="{{ $driver->identifier }}_{{ $identifier }}" value="{{ $palimpsest ? $palimpsest->subConfig($identifier) : '' }}">
                                </div>
                            @endforeach
                        </div>
                    @endforeach
                </div>
                <div class="modal-footer">
                    @if($palimpsest)
                        <a href="#" class="btn btn-danger remote-modal" data-modal-url="{{ route('palimpsest.ask_remove', $palimpsest->id) }}">Delete</a>
                    @endif

                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
