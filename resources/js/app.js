require('./bootstrap');

import jBob from './jbob';
var j = new jBob();

import videojs from 'video.js';

function recordAction(node, action)
{
    var record_cell = node.closest('[data-record-id]');
    var record_id = record_cell.attr('data-record-id');
    var palimpsest_id = node.closest('[data-palimpsest-id]').attr('data-palimpsest-id');
    var active = node.hasClass('active');

    $.ajax({
        url: '/palimpsest/' + action + '/' + palimpsest_id + '/' + record_id + '/' + (active ? '0' : '1'),
        method: 'POST',
        dataType: 'HTML',
        data: {
            _token: $('meta[name="csrf-token"]').attr('content'),
        },
        success: function(new_cell) {
            var nc = $(new_cell);
            record_cell.replaceWith(nc);

            nc.find('video').each(function() {
                initVideoPlayer($(this));
            });
        }
    });
}

function initVideoPlayer(node)
{
    videojs(node.attr('id'), {
        autoplay: 'muted',
        controls: true,
    });
}

$(document).ready(function() {
    j.init({
        initFunction: function(container) {
            container.find('video').each(function() {
                initVideoPlayer($(this));
            });
        },
    });

    $('.add-to-palimpsest').click(function() {
        var palimpsest_id = $(this).closest('[data-palimpsest-id]').attr('data-palimpsest-id');

        $('#edit-record-new').modal('show');

        $('#edit-record-new').find('form').unbind('submit').submit(function(e) {
            e.preventDefault();

            $.ajax({
                url: $(this).attr('action'),
                method: $(this).attr('method'),
                data: $(this).serialize(),
                dataType: 'JSON',
                success: function(data) {
                    var url = '/palimpsest/attach/' + palimpsest_id + '/' + data.id + '/1';
                    $.ajax({
                        url: url,
                        method: 'POST',
                        data: {
                            _token: $('meta[name="csrf-token"]').attr('content'),
                        },
                        success: function(data) {
                            location.reload();
                        }
                    });
                }
            });
        });
    });

    $('.do_loopback').click(function() {
        recordAction($(this), 'loopback');
    });

    $('.do_broadcast').click(function() {
        recordAction($(this), 'broadcast');
    });

    $('body').on('change', 'select[name=driver]', function() {
        $(this).closest('form').find('.params_block').not('.hidden').addClass('hidden');
        $(this).closest('form').find('.params_block_' + $(this).val()).removeClass('hidden');
    });
});
