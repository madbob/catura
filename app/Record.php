<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

use App;
use Log;

use App\Helpers\Loopback;

class Record extends Model
{
    public function destinations()
    {
        return $this->hasMany('App\Destination');
    }

    public function palimpsest()
    {
        return $this->belongsToMany('App\Palimpsest');
    }

    public function getLoopbackUrlAttribute()
    {
        if ($this->status == 'running') {
            foreach($this->destinations as $dest) {
                if ($dest->driver == 'loopback') {
                    return Loopback::viewUrl($dest->config);
                }
            }
        }

        return null;
    }

    public function getIsBroadcastingAttribute()
    {
        return $this->destinations()->where(function($query) {
            $query->where('driver', '!=', 'loopback');
        })->count() != 0;
    }

    public function getStatusIconAttribute()
    {
        switch($this->status) {
            case 'idle':
                return 'icofont-pause';
            case 'running':
                return 'icofont-play-alt-1';
            case 'finished':
                return 'icofont-stop';
        }
    }

    public function triggerDestinations($filename)
    {
        /*
            TODO
        */
    }

    /*
        This has to be executed after Record's Destinations have been updated
    */
    public function reroute()
    {
        $queue = App::make('RecordsQueue');

        if ($this->status != 'running') {
            $queue->add($this);
        }
        else {
            if ($record->destinations()->count() == 0) {
                $queue->stop($this);
            }
            else {
                $queue->updateStream($this);
            }
        }
    }

    public function loopback($do)
    {
        $current_loopback_url = $this->loopback_url;
        if (($do == true && $current_loopback_url != null) || ($do == false && $current_loopback_url == null)) {
            return;
        }

        if ($do) {
            $loopback_destinations = $this->destinations()->where(function($query) {
                $query->where('driver', 'loopback');
            })->count();

            if ($loopback_destinations == 0) {
                $dest = new Destination();
                $dest->driver = 'loopback';
                $dest->config = Str::random(20);
                $dest->record_id = $this->id;
                $dest->save();
            }
        }
        else {
            foreach($this->destinations as $dest) {
                if ($dest->driver == 'loopback') {
                    $dest->delete();
                }
            }
        }

        $this->reroute();
    }

    public static function sanitizeSetup($string)
    {
        if (empty($string)) {
            return '';
        }

        $test = $string;

        $test = preg_replace('/[mousemove|click|sleep]*/', '', $test);
        $test = preg_replace('/ [\.0-9]*/', '', $test);
        if(empty($test)) {
            return $string;
        }
        else {
            Log::debug('Invalid remote command string: ' . $string);
            return '';
        }
    }

    public static function checkAuth($request, $id)
    {
        $record = Record::where('identifier', $id)->first();
        if (is_null($record)) {
            throw new \Exception('Record ID not found', 1);
        }

        $user = $request->user();
        if (is_null($user)) {
            $user = User::where('api_key', $request->input('api_key'))->first();
            if (is_null($user)) {
                throw new \Exception('Unauthorized', 1);
            }
        }

        if ($record->user_id != $user->id) {
            throw new \Exception('Unauthorized', 1);
        }

        return $record;
    }

    public function readRequest($request)
    {
        $this->url = $request->input('url');
        $this->setup = self::sanitizeSetup($request->input('setup', ''));
        $this->status = 'idle';
        $this->save();

        $destinations = $request->input('destinations', []);
        $this->destinations()->delete();

        foreach($destinations as $destination) {
            $destination = json_decode($destination);
            $dest = new Destination();
            $dest->driver = $destination->driver;
            $dest->config = $destination->config;
            $dest->record_id = $this->id;
            $dest->save();
        }

        return $this;
    }
}
