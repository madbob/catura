<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Helpers\Loopback;

class Destination extends Model
{
    public function isLive()
    {
        $drivers = self::drivers();

        foreach($drivers as $driver) {
            if ($driver->identifier == $this->driver) {
                return $driver->is_live;
            }
        }

        return ($this->driver == 'loopback');
    }

    public function gstPipeline($index)
    {
        switch($this->driver) {
            case 'loopback':
                return sprintf('flvmux streamable=true name=mux%d ! rtmpsink location="%s"', $index, Loopback::rtmpUrl($this->config));
                break;

            case 'rtmp':
                return sprintf('flvmux streamable=true name=mux%d ! rtmpsink location="%s"', $index, $this->config);
                break;

            default:
                return '';
                break;
        }
    }

    public static function drivers()
    {
        return [
            (object) [
                'identifier' => 'rtmp',
                'name' => 'RTMP',
                'is_live' => true,
                'params' => [
                    'sendto' => (object) [
                        'label' => 'Send Stream To',
                    ],
                    'readfrom' => (object) [
                        'label' => 'Read Stream From',
                    ],
                ]
            ]
        ];
    }
}
