<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

use App\Services\MainLoopHolder;
use App\Services\PulseAudio;
use App\Services\RecordsQueue;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        $this->app->singleton('MainLoopHolder', function ($app) {
            return new MainLoopHolder();
        });

        $this->app->singleton('PulseAudio', function ($app) {
            return new PulseAudio();
        });

        $this->app->singleton('RecordsQueue', function ($app) {
            return new RecordsQueue();
        });
    }
}
