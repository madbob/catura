<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Log;

class Palimpsest extends Model
{
    public function subConfig($name)
    {
        $obj = json_decode($this->config);
        return $obj->$name ?? '';
    }

    public function records()
    {
        return $this->belongsToMany('App\Record')->withPivot(['sorting', 'notes'])->orderBy('sorting', 'asc');
    }

    public function broadcast($record)
    {
        $palimpsest = $this;
        $already_broadcasting = false;
        $already_in_palimpsest = false;

        foreach($this->records as $r) {
            if ($r->id == $record->id) {
                $already_in_palimpsest = true;
            }

            $destinations = $r->destinations()->where(function($query) use ($palimpsest) {
                $query->where('driver', $palimpsest->driver)->where('config', $palimpsest->subConfig('sendto'));
            })->get();

            if ($destinations->isEmpty() == false) {
                if ($r->id == $record->id) {
                    $already_broadcasting = true;
                    Log::info('Already broadcasting');
                }
                else {
                    foreach($destinations as $d) {
                        $d->delete();
                    }

                    $r->reroute();
                }
            }
        }

        if (!$already_broadcasting) {
            $dest = new Destination();
            $dest->driver = $palimpsest->driver;
            $dest->config = $palimpsest->subConfig('sendto');
            $dest->record_id = $record->id;
            $dest->save();

            $record->reroute();
        }

        if ($already_in_palimpsest == false) {
            $this->records()->attach($record->id);
        }
    }
}
