<?php

namespace App\Services;

use Illuminate\Support\Str;

use Log;

class PulseAudio
{
    private $sinks;

    public function __construct()
    {
    }

    public function initEnv()
    {
        $altered = false;

        $this->sinks = $this->readSinks();

        for($i = 1; $i < 11; $i++) {
            $name = 'Virtual' . $i;
            $found = false;

            foreach($this->sinks as $sink) {
                if ($sink->name == $name) {
                    $found = true;
                    break;
                }
            }

            if ($found == false) {
                $this->createVirtualSink($name);
                $altered = true;
            }
        }

        if ($altered) {
            $this->sinks = $this->readSinks();
        }
    }

    public function getSinks()
    {
        return $this->sinks;
    }

    public function isolateByPid($pids, $sink)
    {
        $inputs = $this->readInputs();

        foreach($inputs as $i) {
            if (in_array($i->pid, $pids)) {
                $this->moveInput($i->id, $sink);
            }
        }
    }

    private function createVirtualSink($name)
    {
        Log::info('Creating virtual audio device ' . $name);
        shell_exec('pacmd load-module module-null-sink sink_name=' . $name);
        shell_exec('pacmd update-sink-proplist ' . $name . ' device.description=' . $name);
    }

    private function moveInput($input, $sink)
    {
        $command = sprintf('pactl move-sink-input %d %d', $input, $sink);
        Log::debug($command);
        exec($command);
    }

    private function readSinks()
    {
        $ret = [];

        $output = shell_exec('pactl list short sinks');
        $rows = explode("\n", $output);

        foreach($rows as $row) {
            $elements = explode("\t", $row);

            if (count($elements) != 5) {
                continue;
            }

            if (Str::startsWith($elements[1], 'Virtual') == false) {
                continue;
            }

            $ret[] = (object) [
                'id' => $elements[0],
                'name' => $elements[1],
                'driver' => $elements[2],
                'sample' => $elements[3],
                'status' => $elements[4],
            ];
        }

        return $ret;
    }

    private function readInputs()
    {
        $ret = [];

        $command = 'pactl list sink-inputs';
        Log::debug($command);
        $output = shell_exec($command);
        $rows = explode("\n", $output);

        $input = '';
        $sink = '';
        $pid = '';
        $skip = false;

        foreach($rows as $row) {
            $row = trim($row);

            if (Str::startsWith($row, 'Sink Input')) {
                $input = preg_replace('/^Sink Input #([0-9]*)$/', '\1', $row);
                $skip = false;
            }
            else if (Str::startsWith($row, 'Sink:')) {
                $sink = preg_replace('/^Sink: ([0-9]*)$/', '\1', $row);
            }
            else if (Str::startsWith($row, 'Corked:')) {
                if ($row == 'Corked: yes') {
                    $skip = true;
                }
            }
            else if (Str::startsWith($row, 'application.process.id')) {
                $pid = preg_replace('/^application.process.id = "([0-9]*)"$/', '\1', $row);

                if ($skip == false) {
                    $ret[] = (object) [
                        'id' => $input,
                        'sink' => $sink,
                        'pid' => $pid,
                    ];
                }

                $input = $sink = $pid = '';
            }
        }

        return $ret;
    }
}
