<?php

namespace App\Services;

class MainLoopHolder
{
    private $loop;

    public function __construct()
    {
        $this->loop = \React\EventLoop\Factory::create();
    }

    public function getLoop()
    {
        return $this->loop;
    }
}
