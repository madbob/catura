<?php

/*
    This has been largely inspired by
    https://malinowski.dev/recording-headless-selenium-tests-to-mp4.html
*/

namespace App\Services;

use Illuminate\Support\Str;

use Log;
use App;

use App\Record;
use App\Destination;

use React\ChildProcess\Process;

class RecordsQueueItem
{
    public $browser_process;
    public $browser_pids;
    public $encode_process;
    public $encode_pids;
    public $xserver_port;
    public $tmux_session;
    public $sound_sink;
    public $recording_filename;
}

class RecordsQueue
{
    private $processes;
    private $loop;
    private $soundcards;

    public function __construct()
    {
        $this->processes = [];
        $this->loop = App::make('MainLoopHolder')->getLoop();
    }

    public function initEnv()
    {
        Record::where('status', '!=', 'idle')->update(['status' => 'idle']);
        Destination::where('driver', 'loopback')->delete();
    }

    private function getUnusedXserverPort()
    {
        do {
            $found = false;
            $port = rand(10, 5000);
            foreach($this->processes as $p) {
                if ($p->xserver_port == $port) {
                    $found = true;
                    break;
                }
            }
        } while($found == true);

        return $port;
    }

    private function getUnusedTmuxSession()
    {
        do {
            $found = false;
            $session = Str::random(10);
            foreach($this->processes as $p) {
                if ($p->tmux_session == $session) {
                    $found = true;
                    break;
                }
            }
        } while($found == true);

        return $session;
    }

    private function processTree($pid)
    {
        $involved = [$pid];

        $command = 'ps xao pid,ppid,comm';
        Log::debug($command);
        $output = shell_exec($command);
        $rows = explode("\n", $output);

        foreach($rows as $row) {
            $row = trim($row);
            preg_match('/^([0-9]*) *([0-9]*) *(.*)$/', $row, $matches);
            if (in_array($matches[2], $involved)) {
                $involved[] = $matches[1];
            }
        }

        return $involved;
    }

    private function isolateSourdCard(&$item)
    {
        $pulse = App::make('PulseAudio');

        $available_cards = $pulse->getSinks();
        $free_card = null;

        foreach($available_cards as $ac) {
            $found = false;

            foreach($this->processes as $p) {
                if ($ac->id == $p->sound_sink) {
                    $found = true;
                    break;
                }
            }

            if ($found == false) {
                $free_card = $ac;
                break;
            }
        }

        if (is_null($free_card)) {
            Log::debug('No available free virtual sound card');
            return null;
        }

        $item->sound_sink = $free_card->id;

        $pulse->isolateByPid($item->browser_pids, $free_card->id);
        return sprintf("pulsesrc device='%s.monitor'", $free_card->name);
    }

    private function startGst($item, $record)
    {
        $audiosrc = $this->isolateSourdCard($item);

        /*
            TODO: all of the gst-launch parameters should be parametrized when
            creating the new Record
        */
        $gst_audio_enc = 'voaacenc bitrate=128000';
        $gst_video_enc = 'x264enc bitrate=5000 tune=zerolatency';
        $gst_fps = 30;

        $gst_destinations = [];
        $mux_wiring = [];
        $destination_index = 1;

        foreach($record->destinations as $destination) {
            if ($destination->isLive()) {
                $gst_destinations[] = $destination->gstPipeline($destination_index);
                $mux_wiring[] = sprintf('audioTee. ! queue ! mux%d. videoTee. ! queue ! mux%d.', $destination_index, $destination_index);
                $destination_index++;
            }
        }

        $command = sprintf("DISPLAY=:%d tmux new-session -d -s %s \
            'gst-launch-1.0 ximagesrc show-pointer=false use-damage=false ! video/x-raw,framerate=%s/1 ! videoconvert ! %s ! video/x-h264,profile=high ! h264parse ! tee name=videoTee \
            %s ! %s ! tee name=audioTee \
            %s \
            %s'", $item->xserver_port, $item->tmux_session, $gst_fps, $gst_video_enc, $audiosrc, $gst_audio_enc, join(' ', $gst_destinations), join(' ', $mux_wiring));

        Log::debug($command);
        $item->encode_process = new Process($command);
        $item->encode_process->start($this->loop);

        $item->encode_pids = $this->processTree($item->encode_process->getPid());

        return $item;
    }

    private function stopGst($item)
    {
        /*
            This sends a Ctrl+C to the given tmux session, in which runs
            gst-launch. Not just SIGTERM it, to permit finalization of
            encoding
        */
        $command = sprintf("tmux send-keys -t %s ^c", $item->tmux_session);
        Log::debug($command);
        exec($command);

        /*
            TODO Wait until all processes on $item->encode_pids are exited
        */

        sleep(10);
    }

    public function add($record)
    {
        $myself = $this;

        $item = new RecordsQueueItem();

        $file_extension = 'mp4';

        $item->xserver_port = $this->getUnusedXserverPort();
        $item->tmux_session = $this->getUnusedTmuxSession();
        $item->recording_filename = tempnam(sys_get_temp_dir(), 'recording_') . '.' . $file_extension;

        /*
            For each Chromium session is assigned a unique user data dir, to
            force it to always open a new session instead reusing an existing
            one
        */

        $command = sprintf('xvfb-run --listen-tcp --server-num %d -s "-ac -screen 0 1920x1080x24" chromium-browser "%s" --window-size=1920,1080 --disable-gpu --start-fullscreen --start-maximized --user-data-dir=/tmp/chrome%d', $item->xserver_port, $record->url, $record->id);
        Log::debug($command);
        $item->browser_process = new Process($command);
        $item->browser_process->start($this->loop);

        $startup_wait = 5;

        sleep(1);
        $startup_wait -= 1;
        $item->browser_pids = $this->processTree($item->browser_process->getPid());

        if (!empty($record->setup)) {
            sleep(1);
            $startup_wait -= 1;

            $command = sprintf('DISPLAY=:%d xdotool %s', $item->xserver_port, $record->setup);
            Log::debug($command);
            exec($command);
        }

        sleep($startup_wait);
        $item = $this->startGst($item, $record);

        $item->browser_process->on('exit', function($exitCode, $termSignal) use ($myself, $record) {
            if (isset($this->processes[$record->id])) {
                Log::debug('Terminated browser');
                $item = $this->processes[$record->id];
                $file = $item->recording_filename;
                unset($myself->processes[$record->id]);
                $myself->rmdir_rec('/tmp/chrome' . $record->id);
                $record->triggerDestinations($file);
            }
            else {
                Log::error('Unable to handle terminated process');
            }
        });

        $this->processes[$record->id] = $item;

        $record->status = 'running';
        $record->save();
    }

    public function stop($record)
    {
        if (isset($this->processes[$record->id])) {
            $item = $this->processes[$record->id];

            $this->stopGst($item);

            foreach($item->browser_pids as $pid) {
                posix_kill($pid, SIGTERM);
            }

            $record->status = 'finished';
            $record->save();

            return true;
        }
        else {
            Log::error('Stopping non existing record');
            return false;
        }
    }

    public function updateStream($record)
    {
        if (isset($this->processes[$record->id])) {
            $item = $this->processes[$record->id];

            $this->stopGst($item);
            $item = $this->startGst($item, $record);

            return true;
        }
        else {
            Log::error('Updating non existing record');
            return false;
        }
    }

    public function act($record, $command)
    {
        if (isset($this->processes[$record->id])) {
            $item = $this->processes[$record->id];

            $command = sprintf('DISPLAY=:%d xdotool %s', $item->xserver_port, $command);
            Log::debug($command);
            exec($command);
        }
        else {
            Log::error('Stopping non existing record');
            return false;
        }
    }

    public function has($record)
    {
        return isset($this->processes[$record->id]);
    }

    private function rmdir_rec($dir)
    {
        if (is_dir($dir)) {
            $files = array_diff(scandir($dir), array('.', '..'));
            foreach ($files as $file) {
                (is_dir("$dir/$file")) ? $this->rmdir_rec("$dir/$file") : unlink("$dir/$file");
            }
            return @rmdir($dir);
        }
        else {
            return @unlink($dir);
        }
    }
}
