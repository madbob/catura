<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

use Hash;

use App\User;

class CreateUser extends Command
{
    protected $signature = 'user:create {name} {email}';
    protected $description = 'Create a new user';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $email = $this->argument('email');

        if (User::where('email', $email)->first() != null) {
            $this->error('User already exists');
            return;
        }

        $user = new User();
        $user->name = $this->argument('name');
        $user->email = $email;
        $user->email_verified_at = date('Y-m-d H:i:s');
        $password = Str::random(10);
        $user->password = Hash::make($password);
        $user->api_key = (string) Str::uuid();
        $user->save();

        $this->info('Password: ' . $password);
        $this->info('API key: ' . $user->api_key);
    }
}
