<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App;

use Psr\Http\Message\ServerRequestInterface;
use Symfony\Bridge\PsrHttpMessage\Factory\HttpFoundationFactory;

use Nyholm\Psr7\Factory\Psr17Factory;
use Symfony\Bridge\PsrHttpMessage\Factory\PsrHttpFactory;
use Symfony\Component\HttpFoundation\Response;

class RunServer extends Command
{
    protected $signature = 'server:run {--port=8080}';
    protected $description = 'Run asyncronous server';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $port = $this->option('port');

        App::make('PulseAudio')->initEnv();
        App::make('RecordsQueue')->initEnv();

        $loop = App::make('MainLoopHolder')->getLoop();
        $kernel = App::make(\Illuminate\Contracts\Http\Kernel::class);

        $symfonyHttpFactory = new HttpFoundationFactory();
        $psr17Factory = new Psr17Factory();
        $psrHttpFactory = new PsrHttpFactory($psr17Factory, $psr17Factory, $psr17Factory, $psr17Factory);

        $server = new \React\Http\Server(function (ServerRequestInterface $request) use ($kernel, $symfonyHttpFactory, $psrHttpFactory) {
            $symfonyRequest = $symfonyHttpFactory->createRequest($request);
            $laravelRequest = \Illuminate\Http\Request::createFromBase($symfonyRequest);

            $laravelResponse = $kernel->handle($laravelRequest);

            $response = $psrHttpFactory->createResponse($laravelResponse);

            return $response;
        });

        $socket = new \React\Socket\Server('0.0.0.0:' . $port, $loop);
        $server->listen($socket);

        $loop->run();
    }
}
