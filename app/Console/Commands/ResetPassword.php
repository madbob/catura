<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Hash;

use App\User;

class ResetPassword extends Command
{
    protected $signature = 'user:password {identifier} {password}';
    protected $description = 'Reset password for a user';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $identifier = $this->argument('identifier');
        $user = User::find($identifier);
        if (is_null($user)) {
            $user = User::where('email', $identifier)->first();
            if (is_null($user)) {
                $this->error('User not found');
                return;
            }
        }

        $password = $this->argument('password');

        $user->password = Hash::make($password);
        $user->save();

        $this->info('Password modified');
    }
}
