<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

use App\Palimpsest;
use App\Record;
use App\Destination;

class PalimpsestController extends Controller
{
    public function index(Request $request)
    {
        $user = $request->user();
        $user->refresh();
        return view('palimpsest.index', compact('user'));
    }

    private function readRequest($palimpsest, $request)
    {
        $palimpsest->name = $request->input('name');
        $palimpsest->driver = $request->input('driver');

        $config = (object) [];

        foreach(Destination::drivers() as $driver) {
            if ($driver->identifier == $palimpsest->driver) {
                foreach($driver->params as $identifier => $param) {
                    $key = sprintf('%s_%s', $palimpsest->driver, $identifier);
                    if ($request->has($key)) {
                        $config->$identifier = $request->input($key);
                    }
                    else {
                        $config->$identifier = '';
                    }
                }

                break;
            }
        }

        $palimpsest->config = json_encode($config);
        return $palimpsest;
    }

    public function store(Request $request)
    {
        $p = new Palimpsest();
        $p->user_id = $request->user()->id;
        $p = $this->readRequest($p, $request);
        $p->save();
        return redirect()->route('palimpsest.index');
    }

    public function update(Request $request, Palimpsest $palimpsest)
    {
        $palimpsest = $this->readRequest($palimpsest, $request);
        $palimpsest->save();
        return redirect()->route('palimpsest.index');
    }

    public function record(Request $request, Palimpsest $palimpsest, $record)
    {
        $record = $palimpsest->records()->where('identifier', $record)->first();
        if ($record) {
            return view('palimpsest.record', compact('record', 'palimpsest'));
        }
    }

    public function handleRecords(Request $request, Palimpsest $palimpsest)
    {
        $user = $request->user();

        if ($request->has('identifier')) {
            $id = $request->input('identifier');
            $record = $palimpsest->records()->where('identifier', $id)->first();
            if ($record) {
                $record->readRequest($request);
                $palimpsest->records()->updateExistingPivot($record->id, ['notes' => $request->input('notes', '')]);
            }
        }
        else {
            $record = new Record();
            $record->identifier = (string) Str::uuid();
            $record->user_id = $user->id;

            $record->readRequest($request);
            $palimpsest->records()->attach([$record->id => ['notes' => $request->input('notes', ''), 'sorting' => $palimpsest->records()->count()]]);
        }
    }

    public function loopback(Request $request, Palimpsest $palimpsest, $record_identifier, $status)
    {
        $record = $palimpsest->records()->where('identifier', $record_identifier)->first();
        if ($record) {
            $record->loopback($status == '1');
            return view('record.cell', compact('record'));
        }
    }

    public function broadcast(Request $request, Palimpsest $palimpsest, $record_identifier)
    {
        $record = $palimpsest->records()->where('identifier', $record_identifier)->first();
        if ($record) {
            $palimpsest->broadcast($record);
            return view('record.cell', compact('record'));
        }
    }

    public function askRemove(Request $request, Palimpsest $palimpsest)
    {
        $user = $request->user();
        return view('palimpsest.askremove', compact('palimpsest', 'user'));
    }

    public function destroy(Palimpsest $palimpsest)
    {
        $palimpsest->delete();
        return redirect()->route('palimpsest.index');
    }
}
