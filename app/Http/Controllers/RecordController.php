<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

use App;
use DB;
use Log;

use App\User;
use App\Record;
use App\Destination;

class RecordController extends Controller
{
    private function getUser($request)
    {
        return User::where('api_key', $request->input('api_key'))->first()->id;
    }

    private function storeRecord(Request $request)
    {
        $validatedData = $request->validate([
            'api_key' => 'required|exists:users',
            'url' => 'required|url',
        ]);

        if ($request->has('identifier')) {
            $id = $request->input('identifier');
            $record = Record::checkAuth($request, $id);
        }
        else {
            $record = new Record();
            $record->identifier = (string) Str::uuid();
            $record->user_id = $this->getUser($request);
        }

        $record->readRequest($request);

        return $record;
    }

    public function init(Request $request)
    {
        try {
            DB::beginTransaction();
            $record = $this->storeRecord($request);
            DB::commit();

            return response()->json([
                'status' => 'ok',
                'id' => $record->identifier,
            ]);
        }
        catch(\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage()
            ]);
        }
    }

    public function start(Request $request)
    {
        try {
            DB::beginTransaction();

            $record = $this->storeRecord($request);

            $queue = App::make('RecordsQueue');
            $queue->add($record);

            DB::commit();

            return response()->json([
                'status' => 'ok',
                'id' => $record->identifier,
            ]);
        }
        catch(\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage()
            ]);
        }
    }

    public function act(Request $request, $id)
    {
        $validatedData = $request->validate([
            'api_key' => 'required|exists:users',
        ]);

        try {
            $record = Record::checkAuth($request, $id);
        }
        catch(\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage()
            ]);
        }

        $queue = App::make('RecordsQueue');
        $command = Record::sanitizeSetup($request->input('command', ''));

        if (!empty($command)) {
            $queue->act($record, $command);
        }

        return response()->json([
            'status' => 'ok',
            'id' => $record->identifier,
        ]);
    }

    public function stop(Request $request, $id)
    {
        $validatedData = $request->validate([
            'api_key' => 'required|exists:users',
        ]);

        try {
            $record = Record::checkAuth($request, $id);
        }
        catch(\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage()
            ]);
        }

        DB::beginTransaction();

        $queue = App::make('RecordsQueue');
        $queue->stop($record);

        DB::commit();

        return response()->json([
            'status' => 'ok',
            'id' => $record->identifier,
        ]);
    }

    public function delete(Request $request, $id)
    {
        try {
            $record = Record::checkAuth($request, $id);
        }
        catch(\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage()
            ]);
        }

        $queue = App::make('RecordsQueue');
        $queue->stop($record);

        $record->delete();

        return response()->json([
            'status' => 'ok',
            'id' => $record->identifier,
        ]);
    }

    public function askRemove(Request $request, Record $record)
    {
        try {
            $record = Record::checkAuth($request, $record->identifier);
        }
        catch(\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage()
            ]);
        }

        $user = $request->user();
        return view('record.askremove', compact('record', 'user'));
    }
}
