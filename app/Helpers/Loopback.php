<?php

namespace App\Helpers;

use Illuminate\Support\Str;

class Loopback
{
    public static function rtmpUrl($identifier)
    {
        return Str::finish(env('RTMP_LOOPBACK_SEND'), '/') . $identifier;
    }

    public static function viewUrl($identifier)
    {
        return Str::finish(env('RTMP_LOOPBACK_GET'), '/') . $identifier . '.m3u8';
    }
}
